import { TourPage } from './app.po';

describe('tour App', () => {
  let page: TourPage;

  beforeEach(() => {
    page = new TourPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
